$(document).ready(function() {

    var Slider = function(container) {
        var $container = $(container);

        var sliderHandler = function(key) {

            if (key == "next") {
                count++;
                console.log("countNext = " + count);
                $container.find(" > li:nth-child("+ count +")").animate({left: 0, opacity: 1}, 'slow').prev().animate({left: "-100%", opacity: 0}, 'slow'); // start  ! 1 !

            } else if(key == "prev") {
                count--;
                console.log("countPrev = " + count);
                //console.log($container.find(" > li:nth-child("+count+") .slider-photo").attr("src"));
                $container.find(" > li:nth-child("+count+")").animate({left: 0, opacity: 1}, 'slow').next().animate({left: "100%", opacity: 0}, 'slow');

            }
        };

        var count = 1;
        console.log("globalCount = " + count);
        var sliderQuantity = $container.children("li").length;

        $container.find(".nav-button").click(function() {
            var checkButton = $(this).data("nav");
            sliderHandler(checkButton);
        });
    };

    $('.slides').each(function(index, element) {
        new Slider(element);
    })
});